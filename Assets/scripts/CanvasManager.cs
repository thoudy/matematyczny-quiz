﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour 
{
	public GameObject mainMenu;
	public GameObject quizMenu;
	public GameObject championMenu;
	public GameObject gameMenu;


	public void enterQuizMenu()
	{
		mainMenu.SetActive (false);
		quizMenu.SetActive (true);
	}
}
