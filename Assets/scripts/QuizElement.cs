﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using SimpleJSON;
using UnityEngine.UI;

public class QuizElement : MonoBehaviour 
{
	public string quizID;
	public string questionsUrl = "https://kidquiz.herokuapp.com/api/quizquestions";
	public List<Question> quizQuestions = new List<Question> ();

	private bool questionsDownloaded = false;

	public GameObject quizPanel;
	public GameObject quizMenu;
	public GameObject backButton;
	public GameObject title;

	//elementy quizu
	public string quizName;
	public string quizDescription;
	public int QuizQuestionNumber = 0;

	void Awake()
	{
		this.GetComponent<Button> ().onClick.AddListener (EnterQuiz);
		WWW www = new WWW(questionsUrl);
		StartCoroutine(WaitForQuestionList(www));

	}

	void Start()
	{
		this.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (0, 0, 0);
		quizPanel = GameObject.Find ("QuizGamePanel");
		quizMenu = GameObject.Find ("QuizPanel");
		backButton = GameObject.Find ("BackToMenuButton");
		title = GameObject.Find ("Title");

		//ustawienie elementów quizu
		quizName = this.gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().text;
		quizDescription = this.gameObject.transform.GetChild (2).gameObject.GetComponent<Text> ().text;
	}

	void Update()	
	{
		if (questionsDownloaded == false) {
			this.GetComponent<Button> ().interactable = false;
		} else {
			this.GetComponent<Button> ().interactable = true;
		}
	}

	IEnumerator WaitForQuestionList(WWW www)
	{
		yield return www;

		if (www.error == null) 
		{
			questionsDownloaded = true;

			var D = JSON.Parse(www.text);
			int questCount = D.Count;
			for(int i = 0; i<questCount; i++) 			
			{
				if (D [i] ["quizId"].Value == this.quizID) 
				{			
					Question newQ = new Question ();
					newQ.correctAnswer = D [i] ["correctAnswer"].Value;
					newQ.questionBody = D [i] ["questionBody"].Value;
					newQ.wrongAnswers.Add (D [i] ["answer1"].Value);
					newQ.wrongAnswers.Add (D [i] ["answer2"].Value);
					newQ.wrongAnswers.Add (D [i] ["answer3"].Value);

					QuizQuestionNumber++;

					quizQuestions.Add (newQ);
				}
			}

			//Debug.Log ("udało sie dotrzeć do pytan");

		} 
		else 
		{			
			Debug.Log("WWW Error: "+ www.error);
		}

	}

	public class Question
	{
		public string questionBody;
		public List<string> wrongAnswers = new List<string> ();
		public string correctAnswer;
	}

	public void EnterQuiz()
	{
		GameObject newQuizStart = quizPanel.transform.GetChild (0).gameObject;
		newQuizStart.SetActive (true);
		GameObject.Find ("QuizTitle").GetComponent<Text> ().text = quizName;
		GameObject.Find ("QuizDescription1").GetComponent<Text> ().text = quizDescription;
		GameObject.Find ("QuizQuestionsNUmber").GetComponent<Text> ().text = "Pytania: " + this.QuizQuestionNumber.ToString ();
		GameObject.Find ("StartQuizButton").GetComponent<Button> ().onClick.AddListener (StartQuiz);
		GameObject.Find ("QuizBody").GetComponent<QuizController> ().totalQuizQuestionNumber = this.QuizQuestionNumber;
		quizMenu.SetActive (false);
		backButton.SetActive (false);
	}

	public void StartQuiz()
	{
		GameObject.Find ("QuizBody").GetComponent<QuizController> ().questions = quizQuestions;
		GameObject.Find ("QuizBody").transform.GetChild (0).gameObject.SetActive (true);
		GameObject.Find ("QuizStartPanel").SetActive (false);
		GameObject.Find ("QuizBody").GetComponent<QuizController> ().SetUpQuiz ();

		quizMenu.SetActive (false);
		title.SetActive (false);

		//Debug.Log ("im in it");
	}
}

