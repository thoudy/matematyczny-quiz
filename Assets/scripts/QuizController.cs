﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class QuizController : MonoBehaviour
{
	//lista pytan dla danego quizu
	public List<QuizElement.Question> questions = new List<QuizElement.Question>();

	public Text questionBody;

	public GameObject quizResults;
	public GameObject quizBody;
	public GameObject mainMenuPanel;

	//obiekt, którego dzieci maja odpowiedzi
	public GameObject questionAnswers;

	public GameObject currentCorrectAnswer;

	public int questionCounter = 0;
	public int correctAnswer = 0;
	public int totalQuizQuestionNumber;

	//resultpanel
	public Text markup;
	public Text result;
	public Text mark;
	public Button backButton;

	bool questionIsLocked = true;

	void Update()
	{

	}

	//METODA ODPOWIEDZIALNA ZA UPDATEOWANIE PYTAN
	public void UpdateQuestion()
	{
		if (EventSystem.current.currentSelectedGameObject.GetComponent<AnswerController> ().isCorrect) {
			correctAnswer++;
			Debug.Log ("poprawna odpowiedź");
			Debug.Log ("ilosc poprawnych: " + correctAnswer);
		}
		else 
		{
			Debug.Log ("bledna odpowiedź");
		}

		if (questionCounter == questions.Count - 1) 
		{
			SendResult (correctAnswer);
		}
		else
		{
			questionCounter++;
			SetNewQuestion (questionCounter);
		}

	}

	public void SetNewQuestion(int questionNumber)
	{
		List<GameObject> wrongAnswers = new List<GameObject> ();
		foreach (Transform T in questionAnswers.transform) {
			wrongAnswers.Add (T.gameObject);
			T.gameObject.GetComponent<Button> ().interactable = false;
		}

		StartCoroutine (UnlockQuestions (wrongAnswers));

		questionBody.text = questions [questionNumber].questionBody;

		int randomCorrectAnswerPosition = (int)Random.Range (0, 4);
		questionAnswers.transform.GetChild (randomCorrectAnswerPosition).gameObject.transform.GetChild (0).gameObject.GetComponent<Text> ().text = questions [questionNumber].correctAnswer;
		questionAnswers.transform.GetChild (randomCorrectAnswerPosition).gameObject.GetComponent<AnswerController> ().isCorrect = true;
		currentCorrectAnswer = questionAnswers.transform.GetChild (randomCorrectAnswerPosition).gameObject;
		wrongAnswers.RemoveAt (randomCorrectAnswerPosition);

		foreach (GameObject g in wrongAnswers) {
			int randomWrongAnswer = Random.Range (0, questions [questionNumber].wrongAnswers.Count);
			print ("qddasdas: " + questions[questionNumber]);
			g.transform.GetChild (0).gameObject.GetComponent<Text> ().text = questions [questionNumber].wrongAnswers [randomWrongAnswer];
			questions [questionNumber].wrongAnswers.RemoveAt (randomWrongAnswer);		
		}
	}

	public void SendResult(int correctAnswerNumber)
	{
		int quizzesDone = PlayerPrefs.GetInt ("QuizDone");
		quizzesDone++;
		PlayerPrefs.SetInt ("QuizDone", quizzesDone);

		float testMark = 0;
		string _markup = "";
		quizResults.SetActive (true);
		float resultNote = (float)correctAnswerNumber / totalQuizQuestionNumber * 100.0f;
		Debug.Log ("Poprawnych: " + correctAnswerNumber +
					"Całkowitych: "+ totalQuizQuestionNumber +
					"ocena koncowa: " + resultNote);

		if (resultNote >= 85) {
			testMark = 5;
			_markup = "Świetna robota! Tak trzymaj!";
		} else if (resultNote >= 75) {
			testMark = 4;
			_markup = "Jest dobrze, dobra robota!";
		} else if (resultNote >= 60) {
			testMark = 3;
			_markup = "Jest OK, ale stać Cię na więcej!";
		}
		else if(resultNote >= 50){
			testMark = 2;
			_markup = "Spróbuj poćwiczyć jeszcze i spróbuj jeszcze raz.";
		}
		else if(resultNote >= 0){
			testMark = 1;
			_markup = "Niestety, musisz jeszcze sporo popracować.";
		}

		markup.text = _markup;
		mark.text = "Ocena: " + testMark.ToString ();

		result.text = resultNote.ToString ("F2") + "%";
		this.gameObject.SetActive (false);
		Debug.Log (questionCounter);
	}

	public void ExitQuiz()
	{
		Application.LoadLevel (Application.loadedLevelName);
	}

	//USTAWIENIE QUIZÓW PO WYBRANIU.
	public void SetUpQuiz()
	{
		correctAnswer = 0;
		print ("odpalamy quiz: " + questionCounter);
		SetNewQuestion (questionCounter);
	}

	IEnumerator UnlockQuestions(List<GameObject> buttonlist)
	{
		foreach (GameObject g in buttonlist) 
		{
			g.GetComponent<Button> ().interactable = true;
		}

		yield return new WaitForSeconds (5);
	}
}

