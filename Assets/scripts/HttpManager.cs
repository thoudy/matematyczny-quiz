﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using SimpleJSON;
using UnityEngine.UI;

public class HttpManager : MonoBehaviour {

	public string QuizesListUrl = "https://kidquiz.herokuapp.com/api/quizzes";
	public quizDescription [] quizes;
	public List<Button> quizList;

	public GameObject MainMenu;
	public GameObject LoadingAnim;
	public GameObject QuizComponent;
	public GameObject QuizPrefab;
	public GameObject QuizPanel;

	void Start ()
	{
		WWW www = new WWW(QuizesListUrl);
		StartCoroutine(WaitForQuizList(www));

		MainMenu.SetActive (false);
		LoadingAnim.SetActive (true);
	}

	IEnumerator WaitForQuizList(WWW www)
	{      
		yield return www;

		if (www.error == null)
		{
			//Debug.Log (www.text);
			var D = JSON.Parse(www.text);
			int quizCount = D.Count;

			//print ("Ilość dostępnych quizów: " + quizCount);

			quizes = new quizDescription[quizCount];
			for(int i = 0; i<quizCount; i++)
			{
				quizes[i] = new quizDescription(D[i]["name"].Value,  D[i]["description"].Value,  D[i]["id"].Value);
				//quizes[i].printQuizInfo();
			}

			MainMenu.SetActive (true);
			LoadingAnim.SetActive (false);

			createQuizes (quizes);

		} else {
			Debug.Log("WWW Error: "+ www.error);
		}
	}

	public class quizDescription
	{
		public string name;
		public string description;
		public string id;

		public quizDescription(string n, string d, string i)
		{
			this.name = n;
			this.description = d;
			this.id = i;
		}

		public void printQuizInfo()
		{
			print ("Quiz Id: " + id);
			print ("Quiz Name: " + name);
			print ("Quiz Descrtiption: " + description);
		}
			
	}

	public void createQuizes(quizDescription[] quizior)
	{

		Vector3 startPos = new Vector3 (-250, 0, 0);
		int count = 1;
		foreach (quizDescription o in quizior) {
			GameObject Quiz = (GameObject)Instantiate (QuizPrefab, QuizPanel.transform);
			Quiz.transform.GetChild (0).gameObject.transform.GetChild (0).gameObject.GetComponent<Text>().text= o.name;
			Quiz.transform.GetChild (0).gameObject.transform.GetChild (1).gameObject.GetComponent<Text> ().text = count.ToString ();
			Quiz.transform.GetChild (0).gameObject.transform.GetChild (2).gameObject.GetComponent<Text>().text= o.description;

			Quiz.transform.GetChild (0).gameObject.GetComponent<QuizElement> ().quizID = o.id;

			Quiz.transform.localPosition = startPos;
			startPos.x += 125;

			if (count % 5 == 0) {
				startPos.x = -250;
				startPos.y -= 125;
			}
			count++;
		}		
	}

	public void EnterCardGame()
	{
		Application.LoadLevel ("memoryGame");
	}

	/*public void setQuizesPosition()
	{
		List<Button> list = gameObject.GetComponent<HttpManager> ().quizList;
		foreach (Button i in list) {		
			//Debug.Log(list[1]);
		}
	}*/

}
