﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour 
{

	public GameObject mainMenu;
	public GameObject quizMenu;
	public GameObject championMenu;
	public GameObject gameMenu;
	public GameObject backToMenuFromQuizButton;
	//public GameObject gameMenuPanel;

	public GameObject musicSettingsButton;
	public GameObject soundSettingsButton;
	public GameObject exitAppButton;

	public Text gamesCompleted;
	public GameObject PlayerProfilePanel;

	public Text quizCompleted;

	bool isMusicOn = true;
	bool isSoundOn = true;

	void Start()
	{
		SetPlayerPrefs ();
	}

	public void enterQuizMenu()
	{
		mainMenu.SetActive (false);
		quizMenu.SetActive (true);
		backToMenuFromQuizButton.SetActive (true);
	}

	public void backFromQuizMenu()
	{
		mainMenu.SetActive (true);
		quizMenu.SetActive (false);
		backToMenuFromQuizButton.SetActive (false);
	}

	public void enterGameMenu()
	{
		mainMenu.SetActive (false);
		gameMenu.SetActive (true);
	}

	public void backFromGameMenu()
	{
		gameMenu.SetActive (false);
		mainMenu.SetActive (true);
	}

	public void muteSound()
	{
		Color newColor = new Color (255,0,0,255);
		Color currentColor = new Color (255, 255, 255, 255);

		if (isSoundOn) {
			isSoundOn = false;
			//this.gameObject.GetComponent<AudioSource> ().mute = true;

			soundSettingsButton.GetComponent<Image> ().color = newColor;

		} else {
			isSoundOn = true;
			//this.gameObject.GetComponent<AudioSource> ().mute = false;

			soundSettingsButton.GetComponent<Image> ().color = currentColor;
		}
	}

	public void muteMusic()
	{
		Color newColor = new Color (255,0,0,255);
		Color currentColor = new Color (255, 255, 255, 255);

		if (isMusicOn) {
			isMusicOn = false;
			this.gameObject.GetComponent<AudioSource> ().mute = true;

			musicSettingsButton.GetComponent<Image> ().color = newColor;

		} else {
			isMusicOn = true;
			this.gameObject.GetComponent<AudioSource> ().mute = false;

			musicSettingsButton.GetComponent<Image> ().color = currentColor;
		}

	}

	public void quitApp()
	{
		Application.Quit ();
	}

	public void EnterCardGame()
	{
		Application.LoadLevel ("memoryGame");
	}

	public void SetPlayerPrefs()
	{
		if (PlayerPrefs.GetInt ("MemoryGamesCompleted") == null) 
		{
			PlayerPrefs.SetInt ("MemoryGamesCompleted", 0);
		}

		if (PlayerPrefs.GetInt ("QuizDone") == null) 
		{
			PlayerPrefs.SetInt ("QuizDone", 0);
		}

		quizCompleted.text = "Ilość ukończonych quizów: " + PlayerPrefs.GetInt ("QuizDone");
		gamesCompleted.text = "Ilość ukończonych gier 'Pamięć': " + PlayerPrefs.GetInt("MemoryGamesCompleted");
	}

	public void EnterPlayerProfile()
	{
		mainMenu.SetActive (false);
		PlayerProfilePanel.SetActive (true);
	}

	public void BackFromPlayerProfile()
	{
		mainMenu.SetActive (true);
		PlayerProfilePanel.SetActive (false);
	}
}
