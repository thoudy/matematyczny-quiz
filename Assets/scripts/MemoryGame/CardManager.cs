﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class CardManager : MonoBehaviour 
{
	public GameObject kanvas;

	public bool isPaired = false;
	public bool isFlipped = false;
	public GameObject pairedTo;

	public Sprite cardValueImage;
	public Sprite cardBackImage;

	public MemoryCardGameScript mcgs;

	void Start()
	{
		mcgs = kanvas.GetComponent<MemoryCardGameScript> ();
		cardBackImage = mcgs.cardBackImage;

		this.gameObject.GetComponent<Button> ().image.sprite = cardBackImage;
	}

	void Update()
	{
		if (!isFlipped) {
			gameObject.GetComponent<Button> ().image.sprite = cardBackImage;
		} else {
			gameObject.GetComponent<Button> ().image.sprite = cardValueImage;
		}
	}

	public void FlipCard()
	{	
		mcgs.moveCounter += 1;	
		isFlipped = true;
		mcgs.uncoveredCards.Add (this.gameObject);
		this.gameObject.GetComponent<Button> ().interactable = false;

		mcgs.flipCounter += 1;

		if (mcgs.flipCounter == 2) 
		{

			if (mcgs.uncoveredCards [0].gameObject.GetComponent<CardManager> ().cardValueImage == mcgs.uncoveredCards [1].gameObject.GetComponent<CardManager> ().cardValueImage) 
			{
				print ("jest para");
				mcgs.uncoveredCards [0].gameObject.transform.SetParent (mcgs.uncoveredCardsHolder.transform);
				mcgs.uncoveredCards [1].gameObject.transform.SetParent (mcgs.uncoveredCardsHolder.transform);

				mcgs.uncoveredCards [0].gameObject.GetComponent<Button> ().interactable = false;
				mcgs.uncoveredCards [1].gameObject.GetComponent<Button> ().interactable = false;

				mcgs.uncoveredCards.Clear ();
				mcgs.flipCounter = 0;

				mcgs.points += 10;
			} 
			else 
			{
				mcgs.points -= 1;
			}

		}

		if (mcgs.flipCounter > 2) 
		{
			foreach (Transform g in mcgs.cardHolder.transform) {
				g.gameObject.GetComponent<CardManager> ().isFlipped = false;
				g.gameObject.GetComponent<Button> ().interactable = true;
			}

			mcgs.uncoveredCards.Clear ();
			mcgs.flipCounter = 0;
			isFlipped = true;
			mcgs.uncoveredCards.Add (this.gameObject);
			mcgs.flipCounter += 1;
		}

		if (mcgs.cardHolder.transform.childCount == 0) 
		{
			GameOver ();
		}
	}

	public void GameOver()
	{
		int tempVal = PlayerPrefs.GetInt ("MemoryGamesCompleted");
		int newVal = tempVal + 1;
		PlayerPrefs.SetInt ("MemoryGamesCompleted", newVal);
		mcgs.gameOverPanel.SetActive (true);
		mcgs.finalMoves.text = "Ilość ruchów: " + mcgs.moveCounter;
		mcgs.finalPoints.text = "Punkty: " + mcgs.points;


	}

}
