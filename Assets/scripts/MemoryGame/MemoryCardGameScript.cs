﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MemoryCardGameScript : MonoBehaviour {

	public class CardPair
	{
		public GameObject element1;
		public GameObject element2;

		public Sprite commonSprite;
	}

	public List<CardPair> pairList = new List<CardPair>();
	public Sprite cardBackImage;
	public Sprite[] cardList = new Sprite[52];
	public List<Sprite> usedCards = new List<Sprite> ();
	public GameObject cardHolder;
	public List<GameObject> allCards = new List<GameObject>();
	public List<GameObject> PickedCardList = new List<GameObject>();
	public List<GameObject> NonPickedCardList = new List<GameObject>();

	public List<GameObject> uncoveredCards = new List<GameObject>();
	public GameObject uncoveredCardsHolder;

	public int flipCounter = 0;
	public int moveCounter = 0;
	public int points = 10;

	public Text pointsText;
	public Text moveText;

	public GameObject gameOverPanel;
	public Text finalPoints;
	public Text finalMoves;

	void Start () 
	{
		CreateCardTable ();

		//playerPrefs
		SetPlayerPrefs();
	}

	void Update()
	{
		pointsText.text = "Ilość punktów: " + points.ToString ();
		moveText.text = "Ilość ruchów: " + moveCounter.ToString ();
	}

	public void CreateCardTable()
	{

		//utworzenie listy kart
		foreach (Transform t in cardHolder.transform) {
		
			allCards.Add (t.gameObject);
			NonPickedCardList.Add (t.gameObject);

			//print ("a "+allCards.Count);
			//print ("b "+NonPickedCardList.Count);
		}

		//utworzenie wymieszanych kart grajacych		
		List<int> usedIndexes = new List<int> ();

		int counter = 0;
		int randomIndex;

		while (counter <= 13)
		{
			randomIndex = (int)Random.Range (0, 26);
			if (usedIndexes.Contains (randomIndex)) {
				continue;
			} else 
			{
				usedIndexes.Add (randomIndex);
				counter++;
			}
		}

		for (int i = 0; i < usedIndexes.Count-1; i++) 
		{
			PickedCardList.Add(cardHolder.transform.GetChild (usedIndexes[i]).gameObject);
			cardHolder.transform.GetChild (usedIndexes [i]).gameObject.GetComponent<CardManager> ().isPaired = true;
		}

		print ("picked list: " + PickedCardList.Count);
		print ("nonpicked list: " + NonPickedCardList.Count);

		foreach (GameObject g in PickedCardList) 
		{
			if (NonPickedCardList.Contains (g)) 
			{
				NonPickedCardList.Remove (g);
			}
		}

		print ("po usunieciu: " + NonPickedCardList.Count);

		//wylosowanie grających obrazków
		List<int> spriteIndexes = new List<int>();
		int spriteCounter = 0;
		int spriteRandomIndex = 0;

		while (spriteCounter <= 13) 
		{
			spriteRandomIndex = (int)Random.Range (0, 52);
			if (spriteIndexes.Contains (spriteRandomIndex)) {
				continue;
			}
			else
			{
				spriteIndexes.Add (spriteRandomIndex);
				spriteCounter++;
			}
		}

		for(int j = 0; j< spriteIndexes.Count; j++)
		{
			usedCards.Add (cardList [spriteIndexes [j]]);
		}

		//utworzenie part kart
		for (int k = 0; k < PickedCardList.Count; k++) 
		{
			CardPair newCardPair = new CardPair ();
			newCardPair.element1 = PickedCardList [k];
			newCardPair.element2 = NonPickedCardList [k];
			newCardPair.commonSprite = usedCards [k];

			newCardPair.element1.GetComponent<CardManager> ().pairedTo = newCardPair.element2;
			newCardPair.element2.GetComponent<CardManager> ().pairedTo = newCardPair.element1;

			newCardPair.element1.GetComponent<CardManager> ().cardValueImage = newCardPair.commonSprite;
			newCardPair.element2.GetComponent<CardManager> ().cardValueImage = newCardPair.commonSprite;
		}
	}

	public void ExitGame()
	{
		Application.LoadLevel ("main");
	}

	public void RestartGame()
	{
		Application.LoadLevel (Application.loadedLevelName);
	}
		
	public void SetPlayerPrefs()
	{
		if (PlayerPrefs.GetInt ("MemoryGamesCompleted") == null) 
		{
			PlayerPrefs.SetInt ("MemoryGamesCompleted", 0);
		}
	}
}
